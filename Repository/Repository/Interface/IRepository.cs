﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Repository.Interface
{
    interface IRepository<T>
    {
        public bool Add(T entity);
        public IEnumerable<T> FindAll();
        public T Find(Expression<Func<T, bool>> predicate);
        public bool Update(T entity);
        public bool Remove(T entity);
    }
}
