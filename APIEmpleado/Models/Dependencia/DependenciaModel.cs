﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIEmpleado.Models.Dependencia
{
    public class DependenciaModel
    {
        public int DependenciaId { get; set; }
        public string Nombre { get; set; }
        public string Sigla { get; set; }
    }
}
