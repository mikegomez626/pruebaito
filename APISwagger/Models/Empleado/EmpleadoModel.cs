﻿using APISwagger.Models.Dependencia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APISwagger.Models.Empleados
{
    public class EmpleadoModel
    {
        public int EmpleadoId { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public int DependenciaId { get; set; }

        public DependenciaModel Dependencia { get; set; }
    }
}
