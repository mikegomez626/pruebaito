﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Models
{
    public class Dependencia
    {
        public int DependenciaId { get; set; }
        public string Nombre { get; set; }
        public string Sigla { get; set; }
    }
}
