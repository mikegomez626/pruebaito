﻿using Microsoft.EntityFrameworkCore;
using Repository.Context;
using Repository.Models;
using Repository.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Repository
{
    public class EmpleadoRepository : IRepository<Empleado>
    {
        private readonly PruebaItoContext context;

        public EmpleadoRepository(PruebaItoContext context)
        {
            this.context = context;
        }

        public bool Add(Empleado entity)
        {
            try
            {
                context.Add(entity);
                context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public Empleado Find(Expression<Func<Empleado, bool>> predicate)
        {
            return context.Empleados.FirstOrDefault(predicate);
        }

        public IEnumerable<Empleado> FindAll()
        {
            return context.Empleados.Include(x => x.Dependencia).ToList();
        }

        public bool Remove(Empleado entity)
        {
            try
            {
                context.Remove(entity);
                context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool Update(Empleado entity)
        {
            try
            {
                context.Update(entity);
                context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}
