﻿using APISwagger.Models.Empleados;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace APISwagger.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmpleadosController : ControllerBase
    {
        private readonly IConfiguration configuration;
        private readonly string apiUrl;
        private readonly ApiRequests apiRequests;

        public EmpleadosController(IConfiguration configuration)
        {
            this.configuration = configuration;
            apiUrl = configuration["APIEmpleado"];
            apiRequests = new ApiRequests(apiUrl);
        }

        // GET: api/<EmpleadosController>
        [HttpGet]
        public async Task<IEnumerable<EmpleadoModel>> Get()
        {
            return await apiRequests.GetRequest<IEnumerable<EmpleadoModel>>("Empleado");
        }

        // GET api/<EmpleadosController>/5
        [HttpGet("{id}")]
        public async Task<EmpleadoModel> Get(int id)
        {
            return await apiRequests.GetRequest<EmpleadoModel>($"Empleado/{id}");
        }

        // POST api/<EmpleadosController>
        [HttpPost]
        public async Task<string> Post([FromBody] EmpleadoModel value)
        {
            return await apiRequests.PostRequest(value, "Empleado");
        }

        // PUT api/<EmpleadosController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<EmpleadosController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
