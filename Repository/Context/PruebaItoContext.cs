﻿using Microsoft.EntityFrameworkCore;
using Repository.Models;

namespace Repository.Context
{
    public partial class PruebaItoContext : DbContext
    {
        public PruebaItoContext()
        {
        }

        public PruebaItoContext(DbContextOptions<PruebaItoContext> options)
            : base(options)
        {
        }

        public DbSet<Empleado> Empleados { get; set; }
        public DbSet<Dependencia> Dependencias { get; set; }
    }
}
