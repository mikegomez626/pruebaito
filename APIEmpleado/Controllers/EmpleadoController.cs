﻿using APIEmpleado.Models.Empleados;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Repository.Context;
using Repository.Models;
using Repository.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace APIEmpleado.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmpleadoController : ControllerBase
    {
        private readonly PruebaItoContext context;
        private readonly IMapper mapper;
        private readonly EmpleadoRepository empleadoRepository;

        public EmpleadoController(PruebaItoContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
            empleadoRepository = new EmpleadoRepository(context);
        }


        // GET: api/<EmpleadoController>
        [HttpGet]
        public IEnumerable<EmpleadoModel> Get()
        {
            var empleados = empleadoRepository.FindAll();
            var model = mapper.Map<IEnumerable<EmpleadoModel>>(empleados);

            return model;
        }

        // GET api/<EmpleadoController>/5
        [HttpGet("{id}")]
        public EmpleadoModel Get(int id)
        {
            var empleado = empleadoRepository.Find(x => x.EmpleadoId == id);
            var model = mapper.Map<EmpleadoModel>(empleado);

            return model;
        }

        // POST api/<EmpleadoController>
        [HttpPost]
        public string Post([FromBody] EmpleadoModel value)
        {
            var model = mapper.Map<Empleado>(value);
            var response = empleadoRepository.Add(model);

            return response.ToString();
        }

        // PUT api/<EmpleadoController>/5
        [HttpPut("{id}")]
        public string Put(int id, [FromBody] EmpleadoModel value)
        {
            var model = empleadoRepository.Find(x => x.EmpleadoId == id);
            var response = empleadoRepository.Update(model);

            return response.ToString();
        }

        // DELETE api/<EmpleadoController>/5
        [HttpDelete("{id}")]
        public string Delete(int id)
        {
            var model = empleadoRepository.Find(x => x.EmpleadoId == id);
            var response = empleadoRepository.Remove(model);

            return response.ToString();
        }
    }
}
