﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace APISwagger
{
    public class ApiRequests
    {
        private readonly string apiUrl;

        public ApiRequests(string apiUrl)
        {
            this.apiUrl = apiUrl;
        }

        public async Task<T> GetRequest<T>(string recurso)
        {
            using var request = new HttpRequestMessage();
            var httpClient = new HttpClient();
            request.Method = new HttpMethod("GET");
            request.RequestUri = new Uri(apiUrl + recurso);

            var response = await httpClient.SendAsync(request, HttpCompletionOption.ResponseHeadersRead).ConfigureAwait(false);
            var responseText = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            var responseCode = response.StatusCode;

            var data = JsonSerializer.Deserialize<T>(responseText);

            return data;
        }

        public async Task<string> PostRequest<T>(T model, string recurso)
        {
            var json = JsonSerializer.Serialize(model);
            StringContent dataString = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            using var request = new HttpRequestMessage();
            var httpClient = new HttpClient();
            request.Method = new HttpMethod("POST");
            request.RequestUri = new Uri(apiUrl + recurso);
            request.Content = dataString;

            var response = await httpClient.SendAsync(request, HttpCompletionOption.ResponseHeadersRead).ConfigureAwait(false);
            var responseText = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

            return responseText;
        }
    }
}
