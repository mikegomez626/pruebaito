﻿using APISwagger.Models.Dependencia;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace APISwagger.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DependenciasController : ControllerBase
    {
        private readonly IConfiguration configuration;
        private readonly string apiUrl;
        private readonly ApiRequests apiRequests;

        public DependenciasController(IConfiguration configuration)
        {
            this.configuration = configuration;
            apiUrl = configuration["APIEmpleado"];
            apiRequests = new ApiRequests(apiUrl);
        }

        // GET: api/<DependenciasController>
        [HttpGet]
        public async Task<IEnumerable<DependenciaModel>> Get()
        {
            return await apiRequests.GetRequest<IEnumerable<DependenciaModel>>("Dependencia");
        }

        // GET api/<DependenciasController>/5
        [HttpGet("{id}")]
        public async Task<DependenciaModel> Get(int id)
        {
            return await apiRequests.GetRequest<DependenciaModel>($"Dependencia/{id}");
        }

        // POST api/<DependenciasController>
        [HttpPost]
        public async Task<string> Post([FromBody] DependenciaModel value)
        {
            return await apiRequests.PostRequest(value, "Dependencia");
        }

        // PUT api/<DependenciasController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<DependenciasController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
