﻿using APIEmpleado.Models.Dependencia;
using APIEmpleado.Models.Empleados;
using AutoMapper;
using Repository.Models;

namespace APIEmpleado.Profiles
{
    public class ApiEmpleadoProfile : Profile
    {
        public ApiEmpleadoProfile()
        {
            CreateMap<EmpleadoModel, Empleado>().ReverseMap();
            CreateMap<DependenciaModel, Dependencia>().ReverseMap();
        }
    }
}
