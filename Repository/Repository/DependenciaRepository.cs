﻿using Microsoft.EntityFrameworkCore;
using Repository.Context;
using Repository.Models;
using Repository.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Repository
{
    public class DependenciaRepository : IRepository<Dependencia>
    {
        private readonly PruebaItoContext context;

        public DependenciaRepository(PruebaItoContext context)
        {
            this.context = context;
        }

        public bool Add(Dependencia entity)
        {
            try
            {
                context.Add(entity);
                context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public Dependencia Find(Expression<Func<Dependencia, bool>> predicate)
        {
            return context.Dependencias.FirstOrDefault(predicate);
        }

        public IEnumerable<Dependencia> FindAll()
        {
            return context.Dependencias.ToList();
        }

        public bool Remove(Dependencia entity)
        {
            try
            {
                context.Remove(entity);
                context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool Update(Dependencia entity)
        {
            try
            {
                context.Update(entity);
                context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}
