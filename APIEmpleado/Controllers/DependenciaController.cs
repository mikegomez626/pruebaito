﻿using APIEmpleado.Models.Dependencia;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Repository.Context;
using Repository.Models;
using Repository.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace APIDependencia.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DependenciaController : ControllerBase
    {
        private readonly PruebaItoContext context;
        private readonly IMapper mapper;
        private readonly DependenciaRepository DependenciaRepository;

        public DependenciaController(PruebaItoContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
            DependenciaRepository = new DependenciaRepository(context);
        }


        // GET: api/<DependenciaController>
        [HttpGet]
        public IEnumerable<DependenciaModel> Get()
        {
            var Dependencias = DependenciaRepository.FindAll();
            var model = mapper.Map<IEnumerable<DependenciaModel>>(Dependencias);

            return model;
        }

        // GET api/<DependenciaController>/5
        [HttpGet("{id}")]
        public DependenciaModel Get(int id)
        {
            var Dependencia = DependenciaRepository.Find(x => x.DependenciaId == id);
            var model = mapper.Map<DependenciaModel>(Dependencia);

            return model;
        }

        // POST api/<DependenciaController>
        [HttpPost]
        public string Post([FromBody] DependenciaModel value)
        {
            var model = mapper.Map<Dependencia>(value);
            var response = DependenciaRepository.Add(model);

            return response.ToString();
        }

        // PUT api/<DependenciaController>/5
        [HttpPut("{id}")]
        public string Put(int id, [FromBody] DependenciaModel value)
        {
            var model = DependenciaRepository.Find(x => x.DependenciaId == id);
            var response = DependenciaRepository.Update(model);

            return response.ToString();
        }

        // DELETE api/<DependenciaController>/5
        [HttpDelete("{id}")]
        public string Delete(int id)
        {
            var model = DependenciaRepository.Find(x => x.DependenciaId == id);
            var response = DependenciaRepository.Remove(model);

            return response.ToString();
        }
    }
}
